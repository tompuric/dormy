from django.template.loader import get_template
from django.template import Context
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response
from django.shortcuts import render
from django.template import RequestContext
from users.models import Students
from django.contrib import auth
#csrf - Cross Sight Request Forgery, a security measure
from django.core.context_processors import csrf
from django.contrib.auth.forms import UserCreationForm
from dormy.views import login
import datetime



def register(request):
	form = {}
	
	errors = []
	if request.method == 'POST':
		form.update(csrf(request))
		form['form'] = UserCreationForm()
		form = UserCreationForm(request.POST)
		if form.is_valid():
			new_student = form.save()
			# need to check if someone is already logged on
			return login(request)
		else:
			print errors
			errors.append(form.errors)
	print {'form': UserCreationForm(), 'errors': errors}
	return render_to_response('registration/register.html',
		{'form': UserCreationForm(), 'errors': errors},
		context_instance=RequestContext(request))


def searchUsers(request):

	return render_to_response('landing.html', {}, context_instance=RequestContext(request))







	



