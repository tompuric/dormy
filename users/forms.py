from django import forms
from models import Students
from django.forms import ModelForm


class RegisterStudent(forms.Form):
	F_NAME = forms.CharField(label='Your First Name')
	L_NAME = forms.CharField(label='Your Last Name')
	EMAIL = forms.EmailField(label='Enter your USYD Email')
	PW = forms.CharField(label='Enter your Password')

# and len(F_NAME.split()) > 1:
	def clean_f_name(self):
		if re.search('[^a-zA-Z-\'',  F_NAME):
			return "Invalid First Name"

	def clean_l_name(self):
		if re.search('[^a-zA-Z-\'',  L_NAME):
			return "Invalid Last Name"

	def clean_username(self):
		if re.seach('[^a-zA-Z-_]', USERNAME):
			return "Invalid Username"

	def clean_email(self):
		if not re.search('^\w+[a-zA-Z0-9_.-]*@\w+(\.\w+){1,3}', EMAIL):
			return "Invalid Email Format"

	def clean_password(self):
		if len(PW) < 8:
			return """Password must be greater than or
			equal to 8 characters in length"""


class EditProfileForm(ModelForm):
	class Meta:
		model = Students
		fields = ['F_NAME', 'L_NAME', 'DEGREE']



class FirstProfileForm(ModelForm):
	class Meta:
		model = Students
		fields = ['F_NAME', 'L_NAME', 'DEGREE', 'EMAIL']



