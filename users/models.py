from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class Students(models.Model):
	user = models.OneToOneField(User)
	F_NAME = models.CharField(max_length=30)
	L_NAME = models.CharField(max_length=30)
	DEGREE = models.CharField(max_length=50)
	# Isn't used but needed for forms
	EMAIL = models.EmailField(max_length=75)

	def __unicode__(self):
		return u'%s %s' % (self.F_NAME, self.L_NAME)

User.student = property(lambda u: Students.objects.get_or_create(user=u) [0])

class Achieves(models.Model):
	ACH_NAME = models.CharField(max_length=30)
	DESCR = models.CharField(max_length=100)
	ACH_POINTS = models.CharField(max_length=4)

	def __unicode__(self):
		return u'%s %s' % (self.ACH_NAME, self.DESCR)


class Stu_Ach(models.Model):
	ACH_ID = models.ForeignKey(Achieves)
	STU_ID = models.ForeignKey(Students)
	TIME = models.DateTimeField(auto_now_add=True)

	def __unicode__(self): 
		return u'%s %s %s' % (self.ACH_ID, self.STU_ID, self.TIME)


# Admins are an offshoot of Users, manually created through the database,
# I.e. Admins cannot be created through the website itself, as a security measure.


