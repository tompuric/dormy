from django.forms import ModelForm

from societies.models import *

class EventsForm(ModelForm):
	class Meta:
		model = Events
		fields = ['NAME', 'DESCR', 'TIME']



class PostsForm(ModelForm):
	class Meta:
		model = Posts
		fields = ['CONTENT']


class EditForm(ModelForm):
	class Meta:
		model = Societies
		fields = ['NAME', 'DESCR', 'IMG']


class CreateClubForm(ModelForm):
	class Meta:
		model = Societies
		fields = ['NAME', 'CODE', 'DESCR']