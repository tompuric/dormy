from django.db import models
from users.models import Students
from time import time

def get_upload_file_name(instance, filename):
	return "club_pics/%s_%s" % (str(time()). replace('.','_'), filename)


# Create your models here.
class Societies(models.Model):
	NAME = models.CharField(max_length=30)
	DESCR = models.CharField(max_length=200)
	OWNER = models.ForeignKey(Students)
	CODE = models.CharField(max_length=8)
	# Not actually needed for this implementation. Should probably change since it has 
	# usyd only application
	USU = models.CharField(max_length=10, blank=True)
	UPG = models.BooleanField()
	IMG = models.ImageField(upload_to=get_upload_file_name, blank=True)

	def __unicode__(self):
		return u'%s %s' % (self.NAME, self.OWNER)


class Stu_Soc(models.Model):
	SOC_ID = models.ForeignKey(Societies)
	STU_ID = models.ForeignKey(Students)
	TIME = models.DateTimeField(auto_now_add=True)
	ROLE = models.CharField(max_length=50)

	def __unicode__(self):
		return u'%s %s %s' % (self.SOC_ID, self.STU_ID, self.TIME)

class Events(models.Model):
	SOC_ID = models.ForeignKey(Societies)
	NAME = models.CharField(max_length=30)
	DESCR = models.CharField(max_length=200)
	TIME = models.DateField(max_length=40)


	def __unicode__(self):
		return u'%s %s' % (self.SOC_ID, self.DESCR)



class Posts(models.Model):
	SOC_ID = models.ForeignKey(Societies)
	CONTENT = models.CharField(max_length=200)
	TIME = models.DateTimeField(auto_now_add=True)

	def __unicode__(self):
		return u'%s %s' % (self.SOC_ID, self.CONTENT)
