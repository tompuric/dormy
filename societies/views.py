# Create your views here.
from django.template.loader import get_template
from django import http
from django.template import Context
from django.http import HttpResponse
from django.http import HttpRequest
from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response
from django.shortcuts import render
from django.template import RequestContext
from societies.models import Societies
from users.models import Students
from django.db.models import Q

def search(request, phrase):

	results = {}
	results2 = {}

	results = Societies.objects.filter(Q(NAME__icontains=phrase) | Q(CODE__icontains=phrase))
	results2 = Students.objects.filter(Q(F_NAME__icontains=phrase) | Q(L_NAME__icontains=phrase))

	return render_to_response('Clubs/search.html', {'clubs': results, 'students': results2, 'query': phrase}, 
		context_instance=RequestContext(request))
