

from django import template
from societies.models import Societies
from societies.models import Stu_Soc
from django.contrib.auth.models import User
from django.contrib import auth
from users.models import Students
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.template import Context
from django.http import HttpRequest
import datetime


from django.utils import simplejson
from dajaxice.decorators import dajaxice_register

@dajaxice_register
def dajaxice_example(request, soc, user):

	socID = soc
	stuID = user

	try:
		validFollow = Stu_Soc.objects.get(SOC_ID_id= socID, STU_ID_id=stuID) 
	except Stu_Soc.DoesNotExist:
		f = Stu_Soc(SOC_ID_id=socID, STU_ID_id=stuID, TIME=datetime.datetime.now(), ROLE='Follower')
		f.save()
		follow = True
	else:
		Stu_Soc.objects.get(SOC_ID_id= socID, STU_ID_id=stuID).delete()

		follow = False


	if follow == True:
		message = "You are Now Following This Club."
	else:
		message = "You have Unfollowed This Club."


	return simplejson.dumps({'message': message})


@dajaxice_register
def deleteSoc(request, soc):

	society = Societies.objects.get(id=soc)
	name = society.NAME
	society.delete()

	message = "The Club has been deleted."
	

	return simplejson.dumps({'message': message})



@dajaxice_register
def changeState(request, soc):

	society = Societies.objects.get(id=soc)
	name = society.NAME
	state = society.UPG

	#if state == True:
	#	society.UPG = False
	#else:
	#	society.UPG = True


	message = "The Club has had its state changed."

	return simplejson.dumps({'message': message})








