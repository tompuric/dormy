from django.conf.urls import patterns, include, url
from dormy.views import landing, home

#The following is for Dajaxice - Kerrod
from dajaxice.core import dajaxice_autodiscover, dajaxice_config
dajaxice_autodiscover()
from django.contrib.staticfiles.urls import staticfiles_urlpatterns




# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'webapp.views.home', name='home'),
    # url(r'^webapp/', include('webapp.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),i

    # Basic Urls
    url(r'^$', landing),
    url(r'^home/$', home),


    # Landing Urls
    url(r'^login/$', 'dormy.views.login'),
 #   url(r'^accounts/auth/$', 'dormy.views.auth_view'),
    url(r'^accounts/login/$', 'dormy.views.login'),
    url(r'^accounts/logout/$', 'dormy.views.logout'),
    url(r'^accounts/loggedin/$', 'dormy.views.loggedin'),
    url(r'^accounts/invalid/$',	'dormy.views.invalid_login'),
 #   url(r'^accounts/register/$', 'users.views.register'),
    url(r'^register/$', 'users.views.register'),
    url(r'^firstTimeUser/$', 'dormy.views.firstTime'),
    url(r'^accounts/register/register_success/$', 'dormy.views.register_success'),


    # Club Urls
    url(r'^clubs/$', 'dormy.views.clubs'),
    url(r'^clubs/(?P<socCode>\w{0,10})/$', 'dormy.views.club_page'),
    url(r'^clubs/find=(?P<phrase>\w{0,30})', 'societies.views.search'),

    # Calendar Urls
    url(r'^cal/$', 'dormy.views.cal'),

    # Profile Urls
    url(r'^profile/(?P<userN>\w{0,20})/$', 'dormy.views.profile_page'),

    url(r'^profile/find$', 'users.views.searchUsers'),

    # Admin Urls
    url(r'^admin/$', 'dormy.views.admin'),



    # Dajaxice Urls - Kerrod
    url(dajaxice_config.dajaxice_url, include('dajaxice.urls')),
)


urlpatterns += staticfiles_urlpatterns()

