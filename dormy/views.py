from django.template.loader import get_template
from django import http
from django.template import Context
from django.http import HttpResponse
from django.http import HttpRequest
from django.http import HttpResponseRedirect
from django.shortcuts import render_to_response
from django.shortcuts import render
from django.template import RequestContext
from users.models import Achieves
from societies.models import Societies
from societies.models import Stu_Soc
from societies.models import Events
from societies.models import Posts
from societies.forms import EventsForm
from societies.forms import PostsForm
from societies.forms import EditForm
from societies.forms import CreateClubForm
from users.forms import Students
from users.forms import EditProfileForm
from users.forms import FirstProfileForm
from django.contrib.auth.models import User
from django.contrib import auth
#csrf - Cross Sight Request Forgery, a security measure
from django.core.context_processors import csrf
from django.contrib.auth.forms import UserCreationForm
from django.template import RequestContext
from django.contrib.auth.decorators import login_required
from django.db.models import Q

# For Posts and Events!
import datetime
from datetime import timedelta
from django.utils import timezone
from ago import human
import time
import pytz
from datetime import datetime



from users.models import Students

# Landing and Log in Views
def landing(request):
	if request.user.is_authenticated():

		return HttpResponseRedirect('home')
	else:
		return render_to_response('Landing/Landing.html',
			context_instance=RequestContext(request))

def login(request):
	errors = []

	if request.method == 'POST':

		#A user should be able to use their email or username...
		username = request.POST.get('username', '')
		password = request.POST.get('password1', '')

		if "@" in username:
			#then it is an email
			try:
   				stu = Students.objects.get(EMAIL__iexact=username) 
			except Students.DoesNotExist:
				return HttpResponseRedirect('/login')
			else:
				us = User.objects.get(id=stu.user_id) 
				username = us.username


		user = auth.authenticate(username=username, password=password)
		
		if user is not None:
			auth.login(request, user)
			return HttpResponseRedirect('/home')
		else:
			errors.append('Sorry, that\'s not a valid username or password')
	return render_to_response('Landing/login.html',
		{'errors': errors},
		context_instance=RequestContext(request))

def register_success(request):
	return render_to_response('Landing/register_success.html')

def loggedin(request):
	return render_to_response('Landing/loggedin.html',
							{'full_name': request.user.username}, context_instance=RequestContext(request))

def invalid_login(request):
	return render_to_response('Landing/invalid_login.html', context_instance=RequestContext(request))

def logout(request):
	auth.logout(request)
	return render_to_response('Landing/logout.html', context_instance=RequestContext(request))



# Profile
@login_required
def home(request):
	if request.user.is_authenticated():

		if request.user.student.EMAIL == "":
			return http.HttpResponseRedirect('/firstTimeUser/')

		User = request.user

		numFol = Stu_Soc.objects.filter(STU_ID=User.id).count()
		numOwn = Societies.objects.filter(OWNER=User.student.id).count()

		if request.method == "POST":

			editForm = EditProfileForm(request.POST, instance=User.student)
			if editForm.is_valid():
				User.student.F_NAME = editForm.cleaned_data['F_NAME']
				User.student.L_NAME = editForm.cleaned_data['L_NAME']
				User.student.DEGREE = editForm.cleaned_data['DEGREE']
				editForm.save()

				return http.HttpResponseRedirect('/home/')
		else:

			editForm = EditProfileForm(instance=User.student)


			#The News Feed. All Pre-processing must be done now, before page is loaded.
			ClubsFoll = Stu_Soc.objects.filter(STU_ID=User.id)
			
			allUpdates = []
			# Grabs all posts from all followed clubs
			for clubid in ClubsFoll:
				updates = Posts.objects.filter(SOC_ID=clubid.SOC_ID)

				for u in updates:
					allUpdates.append(u)

			#Now I need all update from the pages the user owns...
			ClubsOwn = Societies.objects.filter(OWNER_id=request.user.student.id)
			for clubid in ClubsOwn:
				updates = Posts.objects.filter(SOC_ID=clubid.id)

				for u in updates:
					allUpdates.append(u)

			allUpdates.sort(key=lambda r: r.TIME, reverse=True)

			# Time Stamps are Incorrect, but will be fixed later...
			# What is wrong is that it is currently Time Zone Naive, and must be TZ Aware, so for now
			# to avoid errors i just added the .now() function


			for update in allUpdates:
				update.TIME = time.mktime(update.TIME.timetuple())
				# A quick fix to adjust for the +10 hour influence for the TZ being set in sydney
				newTime = datetime.fromtimestamp((update.TIME+36000))
				update.TIME = human(newTime)


			return render_to_response('Profile/Profile.html', {'user': User, 'numFol': numFol, 'numOwn': numOwn, 'editForm': editForm, 'updates': allUpdates},
				context_instance=RequestContext(request))
	else:
		return http.HttpResponseRedirect('/Landing/')

# Clubs
@login_required
def clubs(request):
	User = request.user

	owned = {}
	following = {}

	owned = Societies.objects.filter(OWNER__user__username__icontains=User)
	following = Stu_Soc.objects.filter(STU_ID=User.id)


	if request.method == 'POST':
		clubForm = CreateClubForm(request.POST)
		if clubForm.is_valid():
			newClub = Societies.objects.create(
				NAME=clubForm.cleaned_data['NAME'],
				CODE=clubForm.cleaned_data['CODE'],
				DESCR=clubForm.cleaned_data['DESCR'],
				OWNER=User.student,
				USU=' ',
				UPG=False,
			)

		return http.HttpResponseRedirect('/clubs/%s' % newClub.CODE)
	else:

		clubForm = CreateClubForm()

		return render_to_response('Clubs/clubHome.html', {'username': request.user.username, 'clubForm': clubForm, 'owned': owned, 'following': following},
			context_instance=RequestContext(request))


def club_page(request, socCode):

	code = socCode
	user = request.user

	#First, Check if it is a society that exists against all societies.
	try:
   		soc = Societies.objects.get(CODE__iexact=code) 
	except Societies.DoesNotExist:
    		return render_to_response('NotFound.html', context_instance=RequestContext(request))	
	else:

		if soc.OWNER_id == request.user.student.id:

			followers = Stu_Soc.objects.filter(SOC_ID=soc.id).count()
			allUpdates = []
			updates = Posts.objects.filter(SOC_ID=soc.id)

			for u in updates:
				allUpdates.append(u)
				
			for update in allUpdates:
				update.TIME = time.mktime(update.TIME.timetuple())
				# A quick fix to adjust for the +10 hour influence for the TZ being set in sydney
				newTime = datetime.fromtimestamp((update.TIME+36000))
				update.TIME = human(newTime)

			if request.method == 'POST':
				if 'events' in request.POST:

					eventsF = EventsForm(request.POST)
					if eventsF.is_valid():
						event = Events.objects.create(
							SOC_ID=soc,
							NAME=eventsF.cleaned_data['NAME'],
							DESCR=eventsF.cleaned_data['DESCR'],
							TIME=eventsF.cleaned_data['TIME'],
							)


						postsF = PostsForm()
						editF = EditForm(instance=soc)

						return http.HttpResponseRedirect('/clubs/%s' % code)

					else:
						postsF = PostsForm()
						eventsF = EventsForm()
						editF = EditForm(instance=soc)
						
						msg = "There was an error making an event! Remember, when adding a time, please but it in the following format: YYYY-MM-DD, e.g. 2013-11-06."

						return render_to_response('Clubs/clubPage_owner.html', {'soc': soc, 'user': user, 'followers': followers, 'eventsF': eventsF, 'postsF': postsF, 'editF': editF, 'updates': allUpdates, "error": msg}, 
							context_instance=RequestContext(request))	
				elif 'posts' in request.POST:
					postsF = PostsForm(request.POST)
					if postsF.is_valid():
						post = Posts.objects.create(
							SOC_ID=soc,
							CONTENT=postsF.cleaned_data['CONTENT'],
							)
	
						eventsF = EventsForm()
						editF = EditForm(instance=soc)		
						return http.HttpResponseRedirect('/clubs/%s' % code)		
						#return render_to_response('Clubs/clubPage_owner.html', {'soc': soc, 'user': user, 'followers': followers, 'eventsF': eventsF, 'postsF': postsF, 'editF': editF}, 
						#		context_instance=RequestContext(request))
					else:
						postsF = PostsForm()
						eventsF = EventsForm()
						editF = EditForm(instance=soc)

						msg = "There was an error in making your post...Maybe you type too much, or not enough! (we can't handle silly blank posts D= )"

						return render_to_response('Clubs/clubPage_owner.html', {'soc': soc, 'user': user, 'followers': followers, 'eventsF': eventsF, 'postsF': postsF, 'editF': editF, 'updates': allUpdates, "error": msg}, 
							context_instance=RequestContext(request))	

				elif 'edit' in request.POST:

					editF = EditForm(request.POST, request.FILES, instance=soc)
					if editF.is_valid():
						soc.NAME = editF.cleaned_data['NAME']
						soc.DESCR = editF.cleaned_data['DESCR']
						soc.IMG = editF.cleaned_data['IMG']
						editF.save()

						postsF = PostsForm()
						eventsF = EventsForm()

						return HttpResponseRedirect('/clubs/%s' % code)
						#return render_to_response('Clubs/clubPage_owner.html', {'soc': soc, 'user': user, 'followers': followers, 'eventsF': eventsF, 'postsF': postsF, 'editF': editF}, 
						#	context_instance=RequestContext(request))

					else:
						postsF = PostsForm()
						eventsF = EventsForm()
						editF = EditForm(instance=soc)
						
						msg = "There was an error editing the club! Please make sure that you've entered everything correctly."

						return render_to_response('Clubs/clubPage_owner.html', {'soc': soc, 'user': user, 'followers': followers, 'eventsF': eventsF, 'postsF': postsF, 'editF': editF, 'updates': allUpdates, "error": msg}, 
							context_instance=RequestContext(request))	
			else:

				postsF = PostsForm()
				eventsF = EventsForm()
				editF = EditForm(instance=soc)

				return render_to_response('Clubs/clubPage_owner.html', {'soc': soc, 'user': user, 'followers': followers, 'eventsF': eventsF, 'postsF': postsF, 'editF': editF, 'updates': allUpdates}, 
					context_instance=RequestContext(request))	

		else:

			followers = Stu_Soc.objects.filter(SOC_ID=soc.id).count()
			ownerStu = Students.objects.get(id=soc.OWNER_id) 
			owner = User.objects.get(id=ownerStu.user_id)

			socID = soc.id
			stuID = user.student.user_id
			try:
   				validFollow = Stu_Soc.objects.get(SOC_ID_id= socID, STU_ID_id=stuID) 
			except Stu_Soc.DoesNotExist:
				follow = False;
			else:

				follow = True;


			allUpdates = []
			updates = Posts.objects.filter(SOC_ID=soc.id)

			for u in updates:
				allUpdates.append(u)

			for update in allUpdates:
				update.TIME = time.mktime(update.TIME.timetuple())
				# A quick fix to adjust for the +10 hour influence for the TZ being set in sydney
				newTime = datetime.fromtimestamp((update.TIME+36000))
				update.TIME = human(newTime)

			return render_to_response('Clubs/clubPage.html', {'soc': soc, 'user': user, 'follow': follow, 'followers': followers, 'owner': owner.username, 'updates': allUpdates}, context_instance=RequestContext(request))	

def profile_page(request, userN):


	#First, Check if it is a User that exists against all Users.
	try:
   		user = User.objects.get(username__iexact=userN) 
	except User.DoesNotExist:
    		return render_to_response('NotFound.html', context_instance=RequestContext(request))	
	else:
		#If you ARE the user you're asking to see, then redirect
		if request.user == user:
			numFol = Stu_Soc.objects.filter(STU_ID=user.id).count()
			numOwn = Societies.objects.filter(OWNER=user.id).count()
			return render_to_response('/home', {'user': user, 'numFol': numFol, 'numOwn': numOwn},
				context_instance=RequestContext(request))	
		else:
			
			numFol = Stu_Soc.objects.filter(STU_ID=user.id).count()
			numOwn = Societies.objects.filter(OWNER=user.id).count()


			#I passed through user object, so you should be able to call all attributes through
			# the html page...
			return render_to_response('Profile/ProfileOther.html', {'user': user, 'numFol': numFol, 'numOwn': numOwn},
				context_instance=RequestContext(request))	

 
# Calender
@login_required
def cal(request):

	User = request.user

	#The Events. All Pre-processing must be done now, before page is loaded.
	ClubsFoll = Stu_Soc.objects.filter(STU_ID=User.id)
	
	allEvents = []
	# Grabs all posts from all followed clubs
	for clubid in ClubsFoll:
		events = Events.objects.filter(SOC_ID=clubid.SOC_ID)

		for e in events:
			allEvents.append(e)

	#Now I need all update from the pages the user owns...
	ClubsOwn = Societies.objects.filter(OWNER_id=request.user.student.id)
	for clubid in ClubsOwn:
		events = Events.objects.filter(SOC_ID=clubid.id)

		for e in events:
			allEvents.append(e)

	for event in allEvents:
		event.TIME = time.mktime(event.TIME.timetuple())
		print(event.TIME)

	return render_to_response('Cal/calmain.html', {'username': request.user.username, 'events': allEvents},
		context_instance=RequestContext(request))



def firstTime(request):

	if request.user.is_authenticated():
		if request.user.student.EMAIL == "":
			if request.method == "POST":

				User = request.user

				firstForm = FirstProfileForm(request.POST, instance=User.student)
				if firstForm.is_valid():
					User.student.F_NAME = firstForm.cleaned_data['F_NAME']
					User.student.L_NAME = firstForm.cleaned_data['L_NAME']
					User.student.DEGREE = firstForm.cleaned_data['DEGREE']
					User.student.EMAIL = firstForm.cleaned_data['EMAIL']
					firstForm.save()

				return HttpResponseRedirect('/home') 
			else:

				firstForm = FirstProfileForm()

				return render_to_response('registration/firstTime.html', {'firstForm': firstForm}, context_instance=RequestContext(request))

		else:
			return http.HttpResponseRedirect('/home/')
	else:
		return http.HttpResponseRedirect('/landing/')


def admin(request):

	if request.user.is_authenticated():
		if request.user.is_staff == True:

			socs = Societies.objects.all()


			return render_to_response('admin.html', {'socs': socs, 'user': request.user}, context_instance=RequestContext(request))

		else:
			return HttpResponseRedirect('/home')
	else:

		return HttpResponseRedirect('/landing')




